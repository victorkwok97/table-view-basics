//
//  ViewController.m
//  TableViewTutorial
//
//  Created by Victor Kwok on 13/5/2019.
//  Copyright © 2019 TableViewTutorial. All rights reserved.
//

#import "ViewController.h"
#import "TaskTableViewCell.h"

@interface ViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray <NSArray *> *tasks;
@property (nonatomic, strong) NSArray <NSString *> *weekdays;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.tasks = @[@[@"task 1"], @[@"task 2"], @[@"task 3"], @[@"task 4"], @[@"task 5"], @[@"task 6"], @[@"task 7", @"task 8"]];
    self.weekdays = @[@"Mon", @"Tue", @"Wed", @"Thur", @"Fri", @"Sat", @"Sun"];
}

#pragma mark - UITableViewDelegate and UITableViewDataSource methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return a cell
    TaskTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableViewCell"];
    cell.titleLabel.text = self.tasks[indexPath.section][indexPath.row];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections in this table view
    return self.tasks.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in this table view per section
    return self.tasks[section].count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.weekdays[section];
}

@end
