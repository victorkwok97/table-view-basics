//
//  main.m
//  TableViewTutorial
//
//  Created by Victor Kwok on 13/5/2019.
//  Copyright © 2019 TableViewTutorial. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
