//
//  TaskTableViewCell.m
//  TableViewTutorial
//
//  Created by Victor Kwok on 14/5/2019.
//  Copyright © 2019 TableViewTutorial. All rights reserved.
//

#import "TaskTableViewCell.h"

@implementation TaskTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)switchValueChanged:(UISwitch *)sender {
    self.titleLabel.textColor = sender.on ? UIColor.blackColor : UIColor.yellowColor;
}

@end
