//
//  TaskTableViewCell.h
//  TableViewTutorial
//
//  Created by Victor Kwok on 14/5/2019.
//  Copyright © 2019 TableViewTutorial. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TaskTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UISwitch *stateSwitch;

@end

NS_ASSUME_NONNULL_END
